package br.ucsal.bes20182.testequalidade.restaurante.domain;

public class Item {

	public Integer codigo;

	public String nome;

	public Double valorUnitario;

	public Item(String nome, Double valorUnitario) {
		super();
		this.nome = nome;
		this.valorUnitario = valorUnitario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(Double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public Integer getCodigo() {
		return codigo;
	}

	@Override
	public String toString() {
		return "Item [codigo=" + codigo + ", nome=" + nome + ", valorUnitario=" + valorUnitario + "]";
	}

}
