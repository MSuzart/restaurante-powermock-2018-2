package br.ucsal.bes20182.testequalidade.restaurante.business;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


import br.ucsal.bes20182.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20182.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20182.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.MesaDao;


@PrepareForTest({ RestauranteBO.class, MesaDao.class })
@RunWith(PowerMockRunner.class)

public class RestauranteBOUnitarioTest {

	/**
	 * M�todo a ser testado: public static Integer abrirComanda(Integer
	 * numeroMesa) throws RegistroNaoEncontrado, MesaOcupadaException. Verificar
	 * se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * Lembre-se de verificar a chamada ao ComandaDao.incluir(comanda).
	 * @throws Exception 
	 */

	

	@Test
	public void abrirComandaMesaLivre() throws Exception {

		PowerMockito.mockStatic(MesaDao.class);
		
		PowerMockito.doCallRealMethod().when(RestauranteBO.class, "abrirComanda", 1);
		PowerMockito.mockStatic(ComandaDao.class);

		RestauranteBO.abrirComanda(1);
		ComandaDao.incluir(ComandaDao.obterPorCodigo(1));
	

	}

}
