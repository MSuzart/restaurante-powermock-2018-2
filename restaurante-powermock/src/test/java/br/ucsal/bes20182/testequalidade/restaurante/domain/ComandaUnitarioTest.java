package br.ucsal.bes20182.testequalidade.restaurante.domain;

import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import br.ucsal.bes20182.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal.bes20182.testequalidade.restaurante.exception.MesaOcupadaException;

@PrepareForTest({ Comanda.class })
@RunWith(PowerMockRunner.class)

public class ComandaUnitarioTest {

	/**
	 * M�todo a ser testado: private Double calcularTotal(). Verificar o c�lculo
	 * do valor total, com um total de 4 itens.
	 */

	Comanda comanda;
	Mesa m1;

	@Before
	public void setup() throws MesaOcupadaException, ComandaFechadaException {
		m1 = new Mesa(1);
		comanda = new Comanda(m1);

		comanda.incluirItem(new Item("�gua", 6d), 1);
		comanda.incluirItem(new Item("Bacalhau", 29d), 1);
		comanda.incluirItem(new Item("Carne", 10d), 1);
		comanda.incluirItem(new Item("Doce de Leite", 3d), 1);
	}

	@Test
	public void calcularTotal4Itens() throws Exception {


	
		double totalEsperado = 48d;
		double totalObtido = Whitebox.invokeMethod(comanda, "calcularTotal");

		System.out.println(totalObtido);
		Assert.assertEquals(totalObtido, totalEsperado, 0);

	}

}
